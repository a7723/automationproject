/// <reference types="Cypress" />

import HudlLoginPage from '../pageObjects/HudlLoginPage'
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const hudlLoginPage = new HudlLoginPage()

/*
Reads the cypress.env.json file in the root of the folder.
When running the tests, please either create a cypress.env.json file at the root as shown below
{
    "validEmailId": "youemailid@test.com",
    "validPassword": "your password"
}

OR -  edit the default values below

*/

const validEmailId = Cypress.env('validEmailId') || 'default@hudl.com'
const validPassword = Cypress.env('validPassword') || 'default'

Given('I access the Hudl Login Page', () => {
    hudlLoginPage.begin();

})


And(/^I try to login with valid emailId and password combination$/, () => {
       
    hudlLoginPage.login(validEmailId,validPassword)

})

Then(/^I should be successfully logged on$/, () => {

    hudlLoginPage.checkSuccessfulLogon()
})

And(/^I should be able to see my (.*) and emailId$/, (profileName) => {

    
    hudlLoginPage.checkLoggedOnProfile(profileName, validEmailId)
})

And(/^I try to login with invalid (.*) and (.*) combination$/, (emailId, password) => {
    hudlLoginPage.login(emailId, password)

})

And(/^I chose to reset my password by providing my (.*)$/, (emailId) => {

    hudlLoginPage.forgotPasswordLink(emailId)
})

Then(/^I am shown a success message$/, () => {

    hudlLoginPage.passwordResetSuccess()

})

Then(/^I should see the (.*)$/, (errorMessage) => {

    hudlLoginPage.getErrorMessagetext(errorMessage)

})

Then(/^Log In with Organization button should link to the correct URL$/, () => {
    hudlLoginPage.logInWithOrganizationButton()
})

Then(/^SignUp option should link to the correct URL$/, () => {

    hudlLoginPage.signUp()
})

