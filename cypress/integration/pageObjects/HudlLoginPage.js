class HudlLoginPage {

   
    begin() {

        cy.visit("https://www.hudl.com/login")
    }

    emailId() {
        return cy.get('#email')
    }

    password() {
        return cy.get('#password')
    }


    loginButton() {

        return cy.get('#logIn')
    }

    logInWithOrganizationButton() {

        cy.get('#logInWithOrganization').click()
        cy.url().should('include', '/app/auth/login/organization')
        cy.screenshot()

    }

    forgotPasswordLink() {
        cy.get('#forgot-password-link').click()

    }

    passwordReset(emailId) {

        cy.get('forgot-email').type(this.emailId)
        cy.get("resetBtn").click()
    }

    passwordResetSuccess() {
        
        cy.screenshot()
        cy.get(".reset-sent-container .reset-info >h4").should('have.text', "Check Your Email")
        cy.get(".reset-sent-container .reset-info >p:nth-child(2)").should('have.text', "Click the link in the email to reset your password.")
        cy.get(".reset-sent-container .reset-info >p:nth-child(3)").should('have.text', "If you don't see the email, check your junk or spam folders.")

    }

    signUp() {

        cy.get('.sign-up-trial > a').invoke('attr', 'href').should('eq', '/register/signup')
        cy.get('.sign-up-trial > a').invoke('attr', 'href').then(href => {
            cy.request(href).its('status').should('eq', 200);

        });
    }



    login(emailId, password) {
        if (emailId) { this.emailId().type(emailId) }
        if (password) { this.password().type(password) }
        this.loginButton().click()
        
    }

    getErrorMessagetext(errorMessage) {

        cy.get('.login-error-container >p').should('have.text', errorMessage)
        cy.screenshot()

    }

    checkSuccessfulLogon() {

        cy.url().should('include', '/home')
        cy.screenshot()
    }

    checkLoggedOnProfile(profileName,emailId) {

        cy.get(".hui-globaluseritem__display-name > span").should('have.text', profileName)
        cy.get(".hui-globaluseritem__email").should('have.text', emailId)

        

    }
    /*   acceptCookies() {
           return cy.get('')
       }
   
   */



}
export default HudlLoginPage;
