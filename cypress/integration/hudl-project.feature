@scenario=loginPage
Feature: Login Page Tests for Hudl website
  
  Background: User accesses the hudl login page 
	Given I access the Hudl Login Page


 Scenario Outline: User passes valid username and password
  
 And I try to login with valid emailId and password combination 
 Then I should be successfully logged on
 And I should be able to see my <PofileName> and emailId
 
	
	Examples: 
		|PofileName|
		|Coach J|
 
 
 Scenario Outline: User passes incorrect/blank username and password
  
 And I try to login with invalid <EmailId> and <Password> combination 
 Then I should see the <ErrorMessage>
	
	Examples: 
		|EmailId                     | Password | ErrorMessage | 
		|                            |          |We didn't recognize that email and/or password. Need help?| 
		|incorrectEmailformat        | abc      |We didn't recognize that email and/or password. Need help?|
   		|NonExistantAccount@test.com | abc      |We didn't recognize that email and/or password. Need help?|

 
 Scenario Outline: verify that a user can request for password reset    
  And I chose to reset my password by providing my <EmailId>
  Then I am shown a success message

  	Examples: 
		|EmailId |
		|ExistantAccount@test.com |

 Scenario: verify Log In with organization button is working fine    
  Then Log In with Organization button should link to the correct URL  

 Scenario: verify Sign Up link is correct and returns http 200 response    
  Then SignUp option should link to the correct URL