const report = require('multiple-cucumber-html-reporter');
var datetime = new Date();
var date = datetime.toISOString().slice(0,16).replace(/-/g,"");

report.generate({
	jsonDir: './cypress/cucumber-json',
	reportPath: './cypress/reports/cucumber-htmlreport.html',
    reportName : 'cucumber-report',
	metadata:{
        browser: {
            name: 'chrome',
            version: '87'
        },
        device: 'Local test machine',
        platform: {
            name: 'Windows',
            version: '10'
        }
    },
    customData: {
        title: 'Run info',
        data: [
            {label: 'Project', value: 'CypressAutomation'},            
            {label: 'Execution Start Time', value: date},
            {label: 'Execution End Time', value: date}
        ]
    }
});