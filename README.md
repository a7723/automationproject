# Automation project

This is a cypress based UI automation project to validate the log in page for the website. 

# Scenarios

## UI testing

1. BDD + POM approach using cypress
2. Covers positive and negative scenarios



## Accessibility

1. Axe-core and Cypress integration
   
accessibilityaXETest.js has a sample implementation where axe-core is directly used. This include three tests. The last test uses assertions for checking accessibility violations based on wcag guidelines and in this instance the test fails with erros



# Requisites and installation
The Project will need Node version 12 (or above).
Steps to install

1. Download or Clone the project.
2. run npm i to install dependancies.
3. 'npx cypress run' should run all tests from the command line
4. to use cypress IDE, run 'npx cypress open'. IDE shows up. You can run both the test suites together or click on the individual suites to run specific tests.
5. Standard cucumber options (tags etc) and cypress options (e.g. browser selections can also be used to run tests). For instance to run a set of tagged tests on chrome , the following command can be used.
'npx cypress-tags run --browser chrome --headed e TAGS="@scenario=loginPage'



# Project Structure
 
## Code Files

1. Feature files are under 'cypress/Integration'. It is named 'hudl-project.feature' in this instance
2. Step Defintions for BDD tests are in folders named after features 'or' in 'common' folder (in this instnace it is only the common folder)
3. Accessibility tests are under 'cypress/Integration/examples'and must follow the naming pattern '*Test.js'. We can define any Glob pattern in the cypress.json
4. pageObject folder has the corresponding paoge class for the page under test.
5. Please note that the credentials required for a successful log on to the website are stored in 'cypress.env.json' file. This file is part of .gitignore and hence does not get checked in. Comments in the commonStepDefs.js provide instructions on setting this up when running locally.
6. Dockerfile and docker-compose files are included 

## Reports
Project uses a few reporting options 

1. Cucumber in-built json reports
   Reports generated for feature files only. Json reports can be found under
    'cypress/cucumber-json'

2. multiple-cucumber-html-reporter
   Reports generated for feature files only. HTML report needs following command to get generated.   
   
   'node cucumber-html-report.js'
   
   report can be found under 'cypress/reports/cucumber-htmlreport.html' folder as 'index.html'
   
3. Mochawesome 
   creates individual report files for tests in 'cypress/mochawesome-report'

4. Mochawesome marge.

Following commands will produce the merged 'mochawesome.html' report in 'cypress/reports' folder
(use npx if you are on Windows)

   - '$(npm bin)/mochawesome-merge cypress/mochawesome-report/*.json > cypress/reports/mochawesome.json'
 
   - '$(npm bin)/marge cypress/reports/mochawesome.json --reportDir cypress/reports/ --inline'


5. Custom json report for accessibility tests
   These are created for each test (three) and also provides a full report. Reports will be named with the testname and found in the folder 'reports/accessibility'.


# Dashboard
A key value pair is required for cypress dashboard use. Results of runs that have the '--record' parameter will send the results to the Cypress dashboard . Dashboard feature in Cypress helps with result analytics and consolidated reporting. 

# BrowserStack Integration
Browser stack integration is acheived through the npm package 'browserstack-cypress-cli'.This is maintained by browserstack.

Please note that the browserstack.json file will need to have npm_dependencies defined in run_settings (As provided here). run command "browserstack-cypress run" to run the tests via browserstack. 


# Few things to note

## cypress-cucumber-preprocessor
The project uses both feature files and js file tests. This hybrid mode works fine when calling spec files and also loading the cypress IDE and the command lines (with few tweaks) is able to interpret Glob defined in cypress.json. 

## mochawesome-merge and mochawesome
The reports created will include the feature file tests as well. The count of scenarios, statuses etc match up .However, the BDD steps within scenarios are not listed in the report and they will appear as Mocha code. Test failures will show up as code assertion failures rather than BDD step failure. This is still good to debug. The cucumber report is more likely to contain text in BDD format/style but only picks up feature file tests.


## cucumber-htmlreport
The merged report needs customisation and at present some of the values on the report (Browser, Device, OS etc are hardcoded).There is ofcourse a json report available and in an ideal set up this could be posted over to a test repository for result dashboarding and time series analysis.

